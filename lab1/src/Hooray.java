//Claire Pickhardt
//Lab#1
//Commented Version

//sets up the program by creating a class
public class Hooray {


    //creates the hooray method
    public static void hooray() {

    //creates a while loop that will print "Hooray!" when the value is true
		while(true) {
			System.out.println("Hooray!");
    	} //while

    } //hooray
        //closes while loop without providing a command for when value
        //is false, creating an infinite loop


    //creates a main method that will call upon the hooray method
    public static void main(String[] args) {
		hooray();
    } //main

} //Hooray (class)
//program ends
