//Claire Pickhardt
//Professor Wenskovitch
//COMPSCI 112
//February 23rd, 2015

import java.util.*;

public class Node {

		public int value;
		public Node next;
        public Node previous;


		public Node(int newValue, Node nextNode, Node previousNode) {
			value = newValue;
			next = nextNode;
                Node previous = previousNode;
		} //Node (constructor)

		public int getValue() {
			return value;
		} //getValue

		public void setValue(int newValue) {
			value = newValue;
		} //setValue

		public Node getNext() {
			return next;
		} //getNext

		public void setNext(Node newNext) {
			next = newNext;
		} //setNext

		public boolean hasNext() {
			if (next == null) {
				return false;
			} else {
				return true;
			} //if-else
		} //hasNext

        public Node getPrevious(){
            return previous;
        }//getPrevious

        public void setPrevious(Node previousNode){
            previous = previousNode;
        }//setprevious

        public boolean hasPrevious(){
            if (previous == null){
                return false;
            }
            else {
                return true;
            }
        }
	} //Node (class)

