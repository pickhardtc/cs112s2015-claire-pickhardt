//Claire Pickhardt
//Professor Wenskovitch
//Computer Science 112
//February 23rd, 2015

import java.util.*;

class DoublyLinkedList {


	private Node head;
	private Node tail;
	private int size;

	public DoublyLinkedList() {
		head = null;
		tail = null;
		size = 0;
	} //DoublyLinkedList (constructor)

	public int size() {
		return size;
	} //size

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		} else {
			return false;
		} //if-else
	} //isEmpty

	public void add(int newInt) {
        Node newest = new Node(newInt, null, tail);

        if (isEmpty()){
        head = newest;
        }
        else {
            tail.next = newest;
        }
            newest.previous = tail;
        tail = newest;
        size++;
	} //add (method)

	public int get(int index) {
		if (index > size) {
			return -1000000000;
		} //if-else

		Node current = head;
		for (int i = 0; i < index; i++) {
			current = current.getNext();
		} //for loop

		return current.getValue();

	} //get statement

	public int getFromEnd(int index) {
		Node current = tail;
        for(int i=0; i < index; i++){

            current = current.getPrevious();
        }//for loop
            return current.getValue();


		}//getfromend (method)


	public void remove(int index) {
		Node currentIndex, previousIndex;
		if (index == 0) {
			head = head.getNext();
		} else {
			previousIndex = head;
			currentIndex = head.getNext();

			for (int i = 0; i < index-1; i++) {
				previousIndex = previousIndex.getNext();
				currentIndex = currentIndex.getNext();
			} //for

			previousIndex.setNext(currentIndex.getNext());
            currentIndex.getNext().setPrevious(previousIndex);

			if (index == size-1) {
				tail = previousIndex;
			} //if
		}
                    size--;

	} //remove

	public boolean testPreviousLinks() {
		Node current = tail;
		int count = 1;
		while (current.getPrevious() != null) {
			current = current.getPrevious();
			count++;
		} //while
		return ((current == head) && (count == size));
	} //testPreviousLinks

} //DoublyLinkedList (class)
