//Claire Pickhardt
//Professor Wenskovitch
//Computer Science 112
//23 March 2015

import java.util.*;
import java.io.*;

    public class lab7{
	public static boolean isMatching(String input) {
		Stack<Character> myStack = new Stack<Character>();

		for (int i = 0; i < input.length(); i++) {
			char current = input.charAt(i);

			if (current == '(' || current == '[' || current == '{') {
				myStack.push(current);
			} else {

				if (current == ')') {
					char stackTop = myStack.peek();

					if (stackTop == '(') {
						stackTop = myStack.pop();
					} else {
						return false;
					} //if-else
				} //if

				if (current == ']') {
					char stackTop = myStack.peek();

					if (stackTop == '[') {
						stackTop = myStack.pop();
					} else {
						return false;
					} //if-else
				} //if

				if (current == '}') {
					char stackTop = myStack.peek();

					if (stackTop == '{') {
						stackTop = myStack.pop();
					} else {
						return false;
					} //if-else
                    }


            }}
         return true;
}//boolean


    public static int num(char c){
    if(c=='^')
        return 3;
    if(c=='/'||c=='*')
        return 2;
    if(c=='+'||c=='-')
        return 1;
    return 0;
    }//int constructor
    public static String infixToPostfix(String in){
        String newStack = in+"((a-b)*c)/d";
        Stack<Character> sta = new Stack<Character>();
        sta.push('(');
        int i,j = newStack.length();
        char c;
        String z=" ";

        for(i=0;i<j;i++){
            c=newStack.charAt(i);
            if(Character.isLetter(c)==true)
                z+=c;
            else if(c=='(')
                sta.push(c);
            else if (c==')')
            {while(sta.peek()!='(')
                z+=sta.pop();
                sta.pop();
            }//if-else
            else{
            while(num(c)<=num(sta.peek()))
                z+=sta.pop();
            sta.push(c);
            }//if-else
        }//for loop
    while(!sta.empty()){
    z+=(sta.pop());
    }//while loop
    return z;

    }//infixToPostfix constructor

    public static int PostfixCalc(String z) {
        Stack<Integer> stuck = new Stack<Integer>();
        int num1 = 0;
        int num2 = 0;
        int endProduct = 0;
            String[] strang = z.split(" ");


        for(int i =0; i<strang.length; i++){
            if(Character.isDigit(strang[i].charAt(0))){
            stuck.push(Integer.parseInt(strang[i]));
            }//if statement
            else{
                num1 = stuck.pop();
                num2 = stuck.pop();

            switch(strang[i].charAt(0)){
                case '+':
                    stuck.push(num1+num2);
                    endProduct += num2;
                    break;
                case '-':
                    stuck.push(num1-num2);
                    endProduct -= num2;
                    break;
                case '/':
                    stuck.push(num1 / num2);
                    endProduct /= num2;
                    break;
                case '*':
                    stuck.push(num1 * num2);
                    endProduct *= num2;
                    break;
            }//switch statement
            }//else
        }//for loop

        return stuck.pop();
    }//PostfixCalc constructor




    	public static void main(String args[]) throws Exception {
		String myInput = "()";
		boolean isMatched = isMatching(myInput);
		if (isMatched) {
			System.out.println("TRUE");
		} else {
			System.out.println("FALSE");
		} //if-else
        System.out.println(infixToPostfix(myInput));

        //calculator
        String testUno = "30 15 +";
        String testDos = "7 0 + 1 * 2 +";
        String testTres = "9 4 - 8 -";
            System.out.println(PostfixCalc(testUno));
            System.out.println(PostfixCalc(testDos));
            System.out.println(PostfixCalc(testTres));
	} //main

    }//PostfixConverter (class)




