import java.util.*;

class ListTester {

	public static void main(String args[]) {

		SinglyLinkedList myList = new SinglyLinkedList();

		myList.add(15);
		myList.add(23);
		myList.add(24);
		myList.add(-6);

		//System.out.println(myList.get(2));

		for (int i = 0; i < myList.size(); i++) {
			System.out.println(myList.get(i));
		} //for


		System.out.println("--------------");

		myList.addFirst(3);
		myList.addFirst(2);
		myList.addFirst(1);

		for (int i = 0; i < myList.size(); i++) {
			System.out.println(myList.get(i));
		} //for

		System.out.println("-------------");

		/* 1, 2, 3, 23, 24, -6 */
		myList.remove(3);
		for (int i = 0; i < myList.size(); i++) {
			System.out.println(myList.get(i));
		} //for

		System.out.println("-------------");

		/* 2, 3, 23, 24, -6 */
		myList.remove(0);
		for (int i = 0; i < myList.size(); i++) {
			System.out.println(myList.get(i));
		} //for

		System.out.println("-------------");

		/* 2, 3, 23, 24 */
		myList.remove(4);
		for (int i = 0; i < myList.size(); i++) {
			System.out.println(myList.get(i));
		} //for






		SinglyLinkedList<Double> myList2 = new SinglyLinkedList<Double>();
		myList2.add(12.34);
		myList2.add(-117.6);

		System.out.println("-------------");

		for (int i = 0; i < myList2.size(); i++) {
			System.out.println(myList2.get(i));
		} //for



		SinglyLinkedList<String> myList3 = new SinglyLinkedList<String>();
		myList3.add("this doesn't break this program");

		System.out.println("-------------");

		for (int i = 0; i < myList3.size(); i++) {
			System.out.println(myList3.get(i));
		} //for



		SinglyLinkedList<Integer> myList4 = new SinglyLinkedList<Integer>();
		myList4.add(0);
		myList4.add(1);

		for (int i = 2; i < 47; i++) {
			myList4.add(myList4.get(i-1) + myList4.get(i-2));
		} //for

		System.out.println("-------------");

		for (int i = 0; i < myList4.size(); i++) {
			System.out.println(myList4.get(i));
		} //for

        myList.getRecursive();


/*
		Iterator iter = myList2.iterator();
		while (iter.hasNext()) {
			System.out.println(iter.next());
		} //while
*/

	} //main

} //ListTester (class)
