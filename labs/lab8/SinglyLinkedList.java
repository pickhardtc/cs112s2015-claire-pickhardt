class SinglyLinkedList {

	public static class Node {

		public int value;
		public Node next;

		public Node(int newValue, Node nextNode) {
			value = newValue;
			next = nextNode;
		} //Node (constructor)

		public int getValue() {
			return value;
		} //getValue

		public void setValue(int newValue) {
			value = newValue;
		} //setValue

		public Node getNext() {
			return next;
		} //getNext

		public void setNext(Node newNext) {
			next = newNext;
		} //setNext

		public boolean hasNext() {
			if (next == null) {
				return false;
			} else {
				return true;
			} //if-else
		} //hasNext

	} //Node (class)

	// Add function goes here
	public Node head;
	public Node tail;
	public int size;

	public SinglyLinkedList() {
		head = null;
		tail = null;
		size = 0;
	} //SinglyLinkedList (constructor)

	public int size() {
		return size;
	} //size

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		} else {
			return false;
		} //if-else
	} //isEmpty

	public void add(int newInt) {
		Node newest = new Node(newInt, null);

		if (isEmpty()) {
			head = newest;
		} else {
			tail.setNext(newest);
		} //if

		tail = newest;
		size++;
	} //add

	public void addFirst(int newInt) {
		Node newest = new Node(newInt, head);

		if (isEmpty()) {
			tail = newest;
		} //if

		head = newest;
		size++;
	} //addFirst

    public void addSorted(int newInt){
        Node newerNode = head;

        Node insert = new Node(newInt, null);
        if (head == null){
        head = insert;
        tail = insert;
        }//if statement
        else if(insert.getValue() < head.getValue()){
            addFirst(insert.getValue());
        }//else-if statement
        else if(insert.getValue() > tail.getValue()){
            add(insert.getValue());
        }//2nd else-if statement
        else{
            for(int i = 0; newerNode.getNext().getValue() < newInt; i++){
                    newerNode = newerNode.getNext();
            }//for loop
            insert.setNext(newerNode.getNext());
            newerNode.setNext(insert);
            size++;
        }//else statement
    }//addSorted

	public int get(int index) {
		if (index > size) {
			return -100;
		} //if

		Node current = head;
		for (int i = 0; i < index; i++) {
			current = current.getNext();
		} //for

		return current.getValue();

	} //get
    public int getRecursive(int index){

            return getRecursive(index, 0, head);
    }

    public int getRecursive(int index, Node current, int goal){
        if (index == goal){
            current.getValue();
        }//if
        else {
            return getRecursive(index, goal+1, current.getNext());
        }//else-if

    }//getrecursive

	public void remove(int location) {
		Node currentLocation, previousLocation;

		if (location == 0) {
			head = head.getNext();
		} else {
			previousLocation = head;
			currentLocation = head.getNext();

			for (int i = 0; i < location-1; i++) {
				previousLocation = previousLocation.getNext();
				currentLocation = currentLocation.getNext();
			} //for

			previousLocation.setNext(currentLocation.getNext());

			if (location == size-1) {
				tail = previousLocation;
			} //if

		} //if-else

		size--;
	} //remove

} //SinglyLinkedList (class)
