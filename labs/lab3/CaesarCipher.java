//Claire Pickhardt
//Computer Science 112
//Professor Wenskovitch
//February 9th, 2015

import java.util.*;

public class CaesarCipher
{

    public static void main(String[] args){


            //ENCRYPT

            String message = "HELLOTHERE";
            int distance = 6;
            String encrypt= " ";

            char cha;
             int hold, hold2;


        for(int i = 0; i < message.length(); i++)
        {
            if(message.charAt(i)+distance > 90){
                hold = (message.charAt(i) + distance)-26;
            }//if statement
            else{
                hold = (message.charAt(i)+distance);
            }//else statement

            cha = (char) hold;
            encrypt = encrypt+cha;

        }//for loop

            System.out.println(encrypt);


            //DECRYPT

            String decrypt = "NKRRUZNKXK";
            for(int i = 0; i < decrypt.length(); i++){
                hold2=((decrypt.charAt(i)-distance)-64);

                if(hold2 <= 0){
                    hold = (90 - (hold2*(-1))%26);
                }//if statement

                else{
                    hold = ((hold2%26)+64);
                }//else statement

             cha = (char) hold2;
             message = message + cha;
            }//for loop

                System.out.println(message);
    }//Main method
}//CaesarCipher (class)
