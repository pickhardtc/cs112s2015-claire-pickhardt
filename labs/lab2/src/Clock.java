import java.util.*;

public class Clock
{


    int hour = 0;
    int minute = 0;
    int seconds = 0;
    int AmPm = 0;
    int month = 0;
    int day = 0;
    int year = 0;
    int secondsPerMinute=60;
    int minutesPerHour=60;



    public Clock(int h, int m, int s, int AP, int mo, int d, int y)
    {
    hour = h;
    minute = m;
    seconds = s;
    AmPm = AP;
    month = mo;
    day = d;
    year = y;
    }//Time (contructor)


    public void setHour(int newHour){
    hour = newHour;
    }
    public void setMinute(int newMinute){
    minute = newMinute;
    }
    public void setSeconds(int newSeconds){
    seconds = newSeconds;
    }
    public void setAmPm(int newAmPm){
    AmPm = newAmPm;
    }
    public void setMonth(int newMonth){
    month = newMonth;
    }
    public void setDay(int newDay){
    day = newDay;
    }
    public void setYear(int newYear){
    year = newYear;
    }

    public void setTime(int newHour, int newMinute, int newSeconds, int newAmPm, int newMonth, int newDay, int newYear){
    hour = newHour;
    minute = newMinute;
    seconds = newSeconds;
    AmPm = newAmPm;
    month = newMonth;
    day = newDay;
    year = newYear;
    }

    public int getHour() {
    return hour;}


    public int getMinute() {
    return minute;
    }

    public int getSeconds() {
    return seconds;
    }

    public int getAmPm() {
    return AmPm;
    }

    public int getMonth() {
    return month;
    }


    public int getDay() {
    return day;
    }


    public int getYear() {
    return year;
    }



    public void printTime()
    {System.out.print("The time is "+hour+":"+minute+":"+seconds);
        if (AmPm == 0)
            System.out.println(" AM");
        else
            System.out.println(" PM");
    System.out.println("The date is: "+month+"/"+day+"/"+year);
    }


    public void addTime(int newSeconds, int s, int m, int h)
    {
        seconds += ((seconds+newSeconds)%60);
        if ((seconds+newSeconds)>60){
        minute += (((seconds+newSeconds)/60)%60);}
        if ((((seconds+newSeconds)/60)%60)>60){
        hour += (newSeconds/60);}
    }


}//Clock (class)
