class BST {

	private class BTreeNode {
		private int value;
		private BTreeNode parent;
		private BTreeNode leftChild;
		private BTreeNode rightChild;

		public BTreeNode(int v, BTreeNode p, BTreeNode l, BTreeNode r) {
			value = v;
			parent = p;
			leftChild = l;
			rightChild = r;
		} //BTreeNode (constructor)

		public int getValue() {
			return value;
		} //getValue

		public void setValue(int v) {
			value = v;
		} //set Value

		public BTreeNode getParent() {
			return parent;
		} //getParent

		public void setParent(BTreeNode p) {
			parent = p;
		} //setParent

		public BTreeNode getLeftChild() {
			return leftChild;
		} //getLeftChild

		public void setLeftChild(BTreeNode l) {
			leftChild = l;
		} //setLeftChild

		public BTreeNode getRightChild() {
			return rightChild;
		} //getRightChild

		public void setRightChild(BTreeNode r) {
			rightChild = r;
		} //setRightChild

	} //BTreeNode (internal class)


	// start of BST class
	BTreeNode root;
	int size;

	public BST() {
		root = null;
		size = 0 ;
	} //BST (constructor)

	public int size() {
		return size;
	} //size

	public boolean isEmpty() {
		if (size == 0) {
			return true;
		} else {
			return false;
		} //if-else
	} //isEmpty


	/*
	** This is the setup function for insert
	** It takes a value as input, and calls the recursive insert function starting at the root
	*/
	public boolean insert(int value) {
        if(root == null){
            root = new BTreeNode(value, null, null, null);
            return true;
        }//if
        else
            return insert(value, root);
	} //add (setup)

	/*
	** This is the recursive function for insert
	** It takes a value and a BTreeNode as input, and either adds a node at the current location
	** (if null), or travels to the left or right child to try to insert there
	*/
	public boolean insert(int value, BTreeNode currentLocation) {

            if (value == currentLocation.getValue())
                  return false;
            else if (value <currentLocation.getValue()) {
                  if (currentLocation.getLeftChild() == null) {
                        currentLocation.setLeftChild(new BTreeNode(value, currentLocation, null, null));
                        return true;
                  } else
                        return insert(value, currentLocation.getLeftChild());
            } else if (value > currentLocation.getValue()) {
                  if (currentLocation.getRightChild() == null) {
                        currentLocation.setRightChild(new BTreeNode(value, currentLocation, null, null));
                        return true;
                  } else
                        return insert(value, currentLocation.getRightChild());
            }
            return false;

}//insert function


	 //add (recursive)



	/*
	** This is the setup function for get
	** It takes a value as input, and calls the recursive get function starting at the root
	**
	** If the value isn't found, let's return a -1
	*/
	public int get(int value) {
        BTreeNode current = root;
        return get(value, current);
}//get


	 //get (setup)

	/*
	** This is the recursive function for get
	** It takes a value and a BTreeNode as input, and either finds a node at the current location
	** (if value=currentLocation.getValue()), or travels to the left or right child to try to find it there
	**
	** If the value isn't found, let's return a -1
	*/
	public int get(int value, BTreeNode currentLocation) {
        if(currentLocation == null){
            return -1;
        }//if

        if(currentLocation.getValue() == value){
            return currentLocation.getValue();
        }//if

        int isItLeft = get(value, currentLocation.getLeftChild());

        if (isItLeft == -1){
            return get(value, currentLocation.getRightChild());
        }//if

        else
            return isItLeft;
	} //get (recursive)

	public BTreeNode findNode(BTreeNode current, int toFind) {
		// actual recursive call

		// check to see if our current location exists
		if (current == null) {
			return null;
		} //if

		// other base case, return the node we're looking for
		if (current.getValue() == toFind) {
			return current;
		} //if

		// try the left child path
		BTreeNode isItLeft = findNode(current.getLeftChild(), toFind);

		if (isItLeft == null) {
			// try the right child path
			return findNode(current.getRightChild(), toFind);
		} else {
			return isItLeft;
		} //if-else

	} //findNode



	/*
	** This is the setup function for remove
	** It takes a value as input, and calls the recursive remove function starting at the root
	*/


	/*
	** This is the recursive function for remove
	** It takes a value and a BTreeNode as input, and either removes the node at the current location
	** (if value=currentLocation.getValue()), or travels to the left or right child to try to remove from there
	*/
	public void remove(int value) {
		BTreeNode delNode = findNode(root, value);

		if (delNode == null) {
			throw new NullPointerException(value + " node doesn't exist!");
		} else {

			if (isLeaf(delNode)) {
				delNode.setValue(-1);
			} else if(hasOneChild(delNode)) {
				BTreeNode parent = delNode.getParent();

				if (delNode.getLeftChild() == null) {
					// we have a right child
					delNode.getRightChild().setParent(delNode.getParent());

					if (parent.getLeftChild().getValue() == value) {
						parent.setLeftChild(delNode.getRightChild());
					} else {
						parent.setRightChild(delNode.getRightChild());
					} //if-else

				} else {
					// we have a left child
					delNode.getLeftChild().setParent(delNode.getParent());

					if (parent.getLeftChild().getValue() == value) {
						parent.setLeftChild(delNode.getLeftChild());
					} else {
						parent.setRightChild(delNode.getLeftChild());
					} //if-else

				} //if-else

			} else {
				// the node we want to delete has two children
				BTreeNode currentLocation = delNode.getRightChild();

				while(!isLeaf(currentLocation)) {
					if (currentLocation.getLeftChild() != null) {
						currentLocation = currentLocation.getLeftChild();
					} else {
						currentLocation = currentLocation.getRightChild();
					} //if-else
				} //while

				BTreeNode currentLocParent = currentLocation.getParent();
				if (currentLocParent.getLeftChild().getValue() == currentLocation.getValue()) {
					currentLocParent.setLeftChild(null);
				} else {
					currentLocParent.setRightChild(null);
				} //if-else

				currentLocation.setParent(delNode.getParent());

				BTreeNode parent = delNode.getParent();
				if (parent.getLeftChild().getValue() == value) {
					parent.setLeftChild(currentLocation);
				} else {
					parent.setRightChild(currentLocation);
				} //if-else

				delNode.getLeftChild().setParent(currentLocation);
				delNode.getRightChild().setParent(currentLocation);

				currentLocation.setLeftChild(delNode.getLeftChild());
				currentLocation.setRightChild(delNode.getRightChild());

				delNode = null;


            }   } } //remove (recursive)
     public boolean isLeaf(BTreeNode node) {
		if(node.getLeftChild() == null && node.getRightChild() == null) {
			return true;
		} else {
			return false;
		} //if-else
	} //isLeaf

	public boolean hasOneChild(BTreeNode node) {
		if(node.getLeftChild() == null || node.getRightChild() == null) {
			return true;
		} else {
			return false;
		} //if-else

	} //hasOneChild

} //BST
