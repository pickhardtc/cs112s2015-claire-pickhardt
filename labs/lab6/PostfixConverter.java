//Claire Pickhardt
//Professor Wenskovitch
//Computer Science 112
//9 March 2015

import java.util.*;

    public class PostfixConverter{

    	public static void main(String args[]) {
		String myInput = "()";
		boolean isMatched = isMatching(myInput);
		if (isMatched) {
			System.out.println("TRUE");
		} else {
			System.out.println("FALSE");
		} //if-else
        System.out.println(infixToPostfix(myInput));
	} //main


	public static boolean isMatching(String input) {
		Stack<Character> myStack = new Stack<Character>();

		for (int i = 0; i < input.length(); i++) {
			char current = input.charAt(i);

			if (current == '(' || current == '[' || current == '{') {
				myStack.push(current);
			} else {

				if (current == ')') {
					char stackTop = myStack.peek();

					if (stackTop == '(') {
						stackTop = myStack.pop();
					} else {
						return false;
					} //if-else
				} //if

				if (current == ']') {
					char stackTop = myStack.peek();

					if (stackTop == '[') {
						stackTop = myStack.pop();
					} else {
						return false;
					} //if-else
				} //if

				if (current == '}') {
					char stackTop = myStack.peek();

					if (stackTop == '{') {
						stackTop = myStack.pop();
					} else {
						return false;
					} //if-else
                    }


            }}
         return true;
}//boolean


    public static int num(char c){
    if(c=='^')
        return 3;
    if(c=='/'||c=='*')
        return 2;
    if(c=='+'||c=='-')
        return 1;
    return 0;
    }//int constructor
    public static String infixToPostfix(String in){
        String newStack = in+"(a+b)/(c+d)";
        Stack<Character> sta = new Stack<Character>();
        sta.push('(');
        int i,j = newStack.length();
        char c;
        String z=" ";

        for(i=0;i<j;i++){
            c=newStack.charAt(i);
            if(Character.isLetter(c)==true)
                z+=c;
            else if(c=='(')
                sta.push(c);
            else if (c==')')
            {while(sta.peek()!='(')
                z+=sta.pop();
                sta.pop();
            }//if-else
            else{
            while(num(c)<=num(sta.peek()))
                z+=sta.pop();
            sta.push(c);
            }//if-else
        }//for loop
    while(!sta.empty()){
    z+=(sta.pop());
    }//while loop
    return z;

    }//infixToPostfix constructor
    }//PostfixConverter (class)

