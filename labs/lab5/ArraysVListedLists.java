//Claire Pickhardt
//Professor Wenskovitch
//COMPSCI 112
//2 March 2015

import java.util.*;

public class ArraysVListedLists {

    public static void main(String[] args){

       long arrayStartTime = System.nanoTime();

        int[] myArray = new int[100000];

        for (int i = 0; i < myArray.length; i++) {
        myArray[i] = i;
        } //for
    long arrayEndTime = System.nanoTime();
    long arrayTime = arrayEndTime - arrayStartTime;

        System.out.println("Array: "+arrayTime);

    long myListStartTime = System.nanoTime();

    LinkedList <Integer> myList = new LinkedList<Integer>();

        for (int i = 0; i < 100000; i++) {
        myList.add(i);
        } //for
    long myListEndTime = System.nanoTime();
    long myListTime = myListEndTime - myListStartTime;


     System.out.println("Linked List: "+myListTime);

    long mySListStartTime = System.nanoTime();

    SinglyLinkedList <Integer> mySList = new SinglyLinkedList<Integer>();

        for (int i = 0; i < 100000; i++) {
             mySList.add(i);
         } //for
    long mySListEndTime = System.nanoTime();
    long mySListTime = mySListEndTime - mySListStartTime;

    System.out.println("Singly Linked List: "+mySListTime);

        arrayStartTime = System.nanoTime();
            for (int i = 0; i < myArray.length; i++) {
             int something = myArray[i];
        } //for

     arrayEndTime = System.nanoTime();
     arrayTime = arrayEndTime - arrayStartTime;

        System.out.println("Array time: "+arrayTime);

        myListStartTime = System.nanoTime();
            for (int i = 0; i < myList.size(); i++) {
             int variable = myList.get(i);
        } //for
     myListEndTime = System.nanoTime();
     myListTime = myListEndTime - myListStartTime;


        System.out.println("My List time: "+myListTime);

        mySListStartTime = System.nanoTime();
        for (int i = 0; i < myList.size(); i++) {
        int somethingElse = mySList.get(i);
        } //for
     mySListEndTime = System.nanoTime();
     mySListTime = mySListEndTime - mySListStartTime;

        System.out.println("Singly Linked List time: "+mySListTime);


 } // Main Method

 } // ArraysVLinkedLists (class)
