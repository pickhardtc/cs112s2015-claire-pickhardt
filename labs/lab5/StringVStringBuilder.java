//Claire Pickhardt
//Professor Wenskovitch
//COMPSCI 112
//2 March 2015

import java.util.*;

public class StringVStringBuilder {

    public static void main(String[] args){

    long stringStartTime = System.nanoTime();
    String str = new String();
    for (int i = 0; i < str.length() ; i++) {
        str = str + "abc";
    }//for (String)
    long stringEndTime = System.nanoTime();

    long stringTime = stringEndTime - stringStartTime;


    long stringBuilderStartTime = System.nanoTime();
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < sb.length() ; i++) {
        sb.append("abc");
    }//for (StringBuilder)
    long stringBuilderEndTime = System.nanoTime();

    long stringBuilderTime = stringBuilderEndTime - stringBuilderStartTime;

    System.out.println("String time elapsed: "+stringTime);
    System.out.println("StringBuilder time elapsed: "+stringBuilderTime);
    }//Main (method)
}//StringVStringBuilder (class)
